# embed

```go
import (
	_ "embed"
)

//go:embed sample.conf
var sampleConfig string
------------------------------
╰─± go run main.go
# Read NSQ topic and channel statistics.
[[inputs.nsq]]
## An array of NSQD HTTP API endpoints
endpoints  = ["http://localhost:4151"]

## Optional TLS Config
# tls_ca = "/etc/telegraf/ca.pem"
# tls_cert = "/etc/telegraf/cert.pem"
# tls_key = "/etc/telegraf/key.pem"
## Use TLS but skip chain & host verification
# insecure_skip_verify = false
```

# NSQ 架构

- nsqd: 接收、队列和传送消息到客户端的守护进程.（data节点）
- nsqlookupd: 管理的拓扑信息，并提供了最终一致发现服务的守护进程.(meta节点)
- nsqadmin: Web UI 来实时监控集群(和执行各种管理任务).

单个nsqd实例旨在一次处理多个数据流。流称为“主题”，一个主题有 1 个或多个“频道”。每个通道都会收到一个主题的所有消息的副本。

主题和频道均未预先配置。主题是在指定主题上首次发布或通过订阅指定主题的频道时创建的。频道是在首次使用订阅指定频道时创建的。
并且两者都相互独立地缓冲数据。

一个通道通常确实连接了多个客户端，并且每条消息将被传递到一个随机客户端.

![img.png](./assets/img.png)

# Docker Install 

- docker-compose.yml

```shell
version: '3'
services:
  nsqlookupd:
    image: nsqio/nsq
    command: /nsqlookupd
    ports:
      - "4160:4160"
      - "4161:4161"
  nsqd:
    image: nsqio/nsq
    command:  /nsqd --broadcast-address=nsqd --lookupd-tcp-address=nsqlookupd:4160
    depends_on:
      - nsqlookupd
    ports:
      - "4151:4151"
      - "4150:4150"
  nsqadmin:
    image: nsqio/nsq
    command: /nsqadmin --lookupd-http-address=nsqlookupd:4161
    depends_on:
      - nsqlookupd  
    ports:
      - "4171:4171"
```

在浏览器中打开[http://localhost:4171](http://localhost:4171)它将列出所有可用的主题

- 设置/etc/host

```shell
# nsq nsqlookupd
127.0.0.1 nsqd nsqd
```

- 编译

```shell
go build -o producer producer.go
go build -o consumer consumer.go
```

- 运行

```shell
./consumer
----------------------
2022/07/30 18:13:17 INF    1 [Topic_Example/Channel_Example] querying nsqlookupd http://127.0.0.1:4161/lookup?topic=Topic_Example
2022/07/30 18:13:17 INF    1 [Topic_Example/Channel_Example] (nsqd:4150) connecting to nsqd
2022/07/30 18:13:18 Message

./producer
-----------------
2022/07/30 18:18:19 INF    1 (127.0.0.1:4150) connecting to nsqd
(base) ╭─taoshumin_vendor at taoshumin_vendor的MacBook Pro in ~/go/src/test.io/third/nsq/code on main✘✘✘
╰─± ./producer
2022/07/30 18:18:20 INF    1 (127.0.0.1:4150) connecting to nsqd
(base) ╭─taoshumin_vendor at taoshumin_vendor的MacBook Pro in ~/go/src/test.io/third/nsq/code on main✘✘✘
╰─± ./producer
2022/07/30 18:18:21 INF    1 (127.0.0.1:4150) connecting to nsqd
```
- 效果图
![img.png](assets/img-2.png)


# [如何保证消息至少到达一次](https://nsq.io/overview/design.html)

NSQ保证一条消息至少被传递一次，尽管重复消息是可能的。消费者应该预料到这一点并进行重复数据删除或执行幂等操作。

此保证作为协议的一部分强制执行，并按如下方式工作（假设客户端已成功连接并订阅了主题）：

- 1.客户端表示他们已准备好接收消息
- 2.NSQ发送消息并将数据临时存储在本地（在重新排队或超时的情况下）
- 3.客户端回复 FIN（完成）或 REQ（重新排队）分别指示成功或失败。如果客户端没有回复NSQ将在可配置的持续时间后超时并自动重新排队消息）

这确保了唯一会导致消息丢失的边缘情况是 nsqd进程的不正常关闭。在这种情况下，内存中的任何消息（或任何未刷新到磁盘的缓冲写入）都将丢失。

- 可能存在的问题

```shell
// https://github.com/nsqio/nsq/issues/1378 (您的消费者可以配置为 max-in-flight=1，以便一次只获取一条消息，并等到它调用finish()该消息后再获取另一条消息。)
config.MaxInFlight = 5 // 来设置最大的处理中的消息数量，会根据这个变量计算在是否更新RDY.
```

# [使用 Toxiproxy 在 Golang 中模拟自定义混沌](https://medium.com/tokopedia-engineering/simulating-customized-chaos-in-golang-using-toxiproxy-b913584d88a7)

推荐：此人很厉害[https://medium.com/@timothyagustian](https://medium.com/@timothyagustian)。


# In Summary

- [https://medium.com/gitconnected/implementing-messaging-queue-nsq-in-golang-using-docker-99b402293b12](https://medium.com/gitconnected/implementing-messaging-queue-nsq-in-golang-using-docker-99b402293b12)


- [https://medium.com/tokopedia-engineering/simulating-customized-chaos-in-golang-using-toxiproxy-b913584d88a7](https://medium.com/tokopedia-engineering/simulating-customized-chaos-in-golang-using-toxiproxy-b913584d88a7)
